package com.platform.elawsuit.entity;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class SearchPageUtil {

    // 查询对象
    private Object object;
    // 排序字段
    private String[] orderBys;
    // 开始行
    private int startRow;
    // 终止行
    private int pageSize;
    // 条件字符串
    private String filter;
    // 排序字符串
    private String orderBy;

    // 第几页
    private int pageIndex;
    // 总数
    private int rowTotal;
    // 总共多少页
    private int pageTotal;

    HttpServletRequest request;

    Integer newsType;

    Map map = null;

    public Map getMap() {
        return map;
    }

    public Integer getNewsType() {
        return newsType;
    }

    public void setNewsType(Integer newsType) {
        this.newsType = newsType;
    }

    public int getRowTotal() {

        return rowTotal;
    }
    public void setRowTotal(int rowTotal) {

        this.rowTotal = rowTotal;
    }
    public int getPageIndex() {

        return pageIndex;
    }
    public void setPageIndex(int pageIndex) {

        this.pageIndex = pageIndex;
    }
    public int getPageTotal() {
        return pageTotal;
    }
    public void setPageTotal(int pageTotal) {
        this.pageTotal = pageTotal;
    }
    public Object getObject() {

        return object;
    }
    public void setObject(Object object) {

        this.object = object;
    }
    public int getStartRow() {
        return startRow;
    }
    public void setStartRow(int startRow) {

        this.startRow = startRow;
    }
    public String getFilter() {

        return filter;
    }
    public int getPageSize() {

        return pageSize;
    }
    public void setPageSize(int pageSize) {

        this.pageSize = pageSize;
    }
    public void setFilter(String filter) {

        this.filter = filter;
    }
    public String[] getOrderBys() {

        return orderBys;
    }
    public void setOrderBys(String[] orderBys) {

        this.orderBys = orderBys;
    }
    public String getOrderBy() {

        return orderBy;
    }
    public void setOrderBy(String orderBy) {

        this.orderBy = orderBy;
    }

    public SearchPageUtil(HttpServletRequest request){
        this.request = request;
        Map properties = request.getParameterMap();
        map = new HashMap();
        Iterator entries = properties.entrySet().iterator();
        Map.Entry entry;
        String name = "";
        String value = "";
        while (entries.hasNext()) {
            entry = (Map.Entry) entries.next();
            name = (String) entry.getKey();
            Object valueObj = entry.getValue();
            if(null == valueObj){
                value = "";
            }else if(valueObj instanceof String[]){
                String[] values = (String[])valueObj;
                for(int i=0;i<values.length;i++){
                    value = values[i] + ",";
                }
                value = value.substring(0, value.length()-1);
            }else{
                value = valueObj.toString();
            }
            map.put(name, value);
        }
        if(request.getParameter("currentPage") != null){
            this.pageIndex = Integer.valueOf(request.getParameter("currentPage"));
            this.startRow = (Integer.valueOf(request.getParameter("currentPage")) - 1) * Integer.valueOf(request.getParameter("pageSize"));
        }
        if (request.getParameter("pageSize") != null){
            this.pageSize = Integer.valueOf(request.getParameter("pageSize"));
        }

    }
    public SearchPageUtil(){

    }
}

package com.platform.elawsuit.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.platform.elawsuit.dao.TestAMapper;
import com.platform.elawsuit.entity.TestA;
import com.platform.elawsuit.service.ITestAService;
import org.springframework.stereotype.Service;

@Service
public class TestAServiceImpl extends ServiceImpl<TestAMapper,TestA> implements ITestAService {
}

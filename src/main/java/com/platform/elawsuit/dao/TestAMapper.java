package com.platform.elawsuit.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.platform.elawsuit.entity.TestA;

import java.util.List;

public interface TestAMapper extends BaseMapper<TestA>{
    public List<TestA> testList(Pagination page,String name);
}

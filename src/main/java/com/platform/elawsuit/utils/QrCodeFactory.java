package com.platform.elawsuit.utils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public class QrCodeFactory {
    private int height;
    private int width;

    public QrCodeFactory(int height, int width) {
        this.height = height;
        this.width = width;
    }

    public void createQrcode(String content,OutputStream outputStream) throws Exception {
        Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        BitMatrix bitMatrix = new MultiFormatWriter().encode(content,
                BarcodeFormat.QR_CODE, width, height, hints);// 生成矩阵
        MatrixToImageWriter.writeToStream(bitMatrix, "png", outputStream);
    }

    public void createQrcodeGBK(String content,OutputStream outputStream) throws Exception {
        Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
        hints.put(EncodeHintType.CHARACTER_SET, "GBK");
        BitMatrix bitMatrix = new MultiFormatWriter().encode(content,
                BarcodeFormat.QR_CODE, width, height, hints);// 生成矩阵
        MatrixToImageWriter.writeToStream(bitMatrix, "png", outputStream);
    }

    public static void main(String[] args) throws Exception{
        QrCodeFactory qrCodeFactory = new QrCodeFactory(189, 189);
        FileOutputStream fos = new FileOutputStream("D:/5/123.png");
        qrCodeFactory.createQrcode("hahaha", fos);
    }
}

package com.platform.elawsuit.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.platform.elawsuit.entity.TestA;
import com.platform.elawsuit.service.ITestAService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("testA")
public class TestAController {

    @Autowired
    private ITestAService testAService;

    @RequestMapping("")
    public ModelAndView index(ModelAndView mv, Page<TestA> page) {
        mv.setViewName("testABrowse");
        mv.addObject("list",testAService.selectList(null));
        return mv;
    }
}